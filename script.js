function calculateSubnet() {
    const ipAddress = document.getElementById("ip-address").value;
    const subnetMask = document.getElementById("subnet-mask").value;

    const ipParts = ipAddress.split(".").map(part => parseInt(part, 10));
    const maskParts = subnetMask.split(".").map(part => parseInt(part, 10));

    if (ipParts.length !== 4 || maskParts.length !== 4) {
        alert("Invalid IP Address or Subnet Mask");
        return;
    }

    const networkAddress = calculateNetworkAddress(ipParts, maskParts);
    const firstUsableAddress = calculateFirstUsableAddress(networkAddress);
    const lastUsableAddress = calculateLastUsableAddress(networkAddress);
    const broadcastAddress = calculateBroadcastAddress(networkAddress);

    const result = `
        <p>IP Address: ${ipAddress}</p>
        <p>Subnet Mask: ${subnetMask}</p>
        <p>Subnet Address: ${networkAddress.join(".")}</p>
        <p>First Usable Address: ${firstUsableAddress.join(".")}</p>
        <p>Last Usable Address: ${lastUsableAddress.join(".")}</p>
        <p>Broadcast Address: ${broadcastAddress.join(".")}</p>
    `;

    document.getElementById("results").innerHTML = result;
}

function calculateNetworkAddress(ipParts, maskParts) {
    const networkAddress = [];
    for (let i = 0; i < 4; i++) {
        networkAddress[i] = ipParts[i] & maskParts[i];
    }
    return networkAddress;
}

function calculateFirstUsableAddress(networkAddress) {
    const firstUsableAddress = [...networkAddress];
    firstUsableAddress[3] += 1;
    return firstUsableAddress;
}

function calculateLastUsableAddress(networkAddress) {
    const lastUsableAddress = [...networkAddress];
    lastUsableAddress[3] += 254;
    return lastUsableAddress;
}

function calculateBroadcastAddress(networkAddress) {
    const broadcastAddress = [...networkAddress];
    broadcastAddress[3] += 255;
    return broadcastAddress;
}

